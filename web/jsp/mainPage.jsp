<%--
  Created by IntelliJ IDEA.
  User: may
  Date: 22/05/2018
  Time: 3:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <%--<link rel="stylesheet" href="css/bootstrap.min.css">--%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>


        #editTextArea{
            display: none;
        }

.save{
    display: none;
}

.comment{
    /*for styling comments in the table*/
    width: 250px; height:35px;background-color: #3B5998; margin: 5px;border-radius: 20px;
    text-align: center;
}

        .smallCommentBtn{
            width: 100px;
            height: 35px;
            font-size: 10px;
            display: inline;
        }

        .tinyComment{
            transform: translate(25px,0px);
            width: 85px; height:35px;background-color: #3B5998;border-radius: 20px
        }
        .tinyCommentBtn{
            transform: translate(10px,0px);
            width: 75px;
            height: 25px;
        }

        .smallText{
            display: inline;
            height: 35px;
        }
        .tinyText{
            width: 75px;
            height: 25px;
            display: inline;
        }



        body {
            background-color: #efefef;
        }
        .container {
            width: 90%;
        }

        /* Nav start */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
            background-color: white;
            color: black;
            font-size: 1.3em;
            border: 0;
        }

        .navbar-brand {
            float: left;
            min-height: 60px;
            padding: 14px 60px 15px;
        }

        .navbar-inverse .navbar-nav .active a,
        .navbar-inverse .navbar-nav .active a:focus,
        .navbar-inverse .navbar-nav .active a:hover
        {
            margin-bottom: 3px;
            border: 3px solid black;
            background-color: white;
        }

        .navbar-collapse {
            padding-right: 30px;
        }

        .navbar-nav>li>a {
            padding-top: 15px;
            padding-bottom: 15px;
            margin: 5px;
            color: red;
        }

        .navbar-toggle {
            position: relative;
            float: right;
            padding: 9px 10px;
            margin-top: 12px;
            margin-right: 15px;
            margin-bottom: 8px;
        }



        @media screen and (max-width:600px) {
            .navbar-brand {
                padding: 14px 35px 15px
            }
        }

        /* Nav end */

        .articleCard {
            background-color: white;
            height: 300px;
            padding: 16px;
            border-radius: calc(0.5rem - 1px);
        }
    </style>
</head>
<body>

<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"></a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">My blog</a></li>
            </ul>
        </div>
    </nav>
    <br>
    <br>
</header>

<div class="container container-margin">
<c:choose>
    <c:when test="${fn:length(articles) gt 0}">

        <c:forEach var="article" items="${articles}">

            <%--<div class="panel panel-default">--%>

            <%--<h3 class="panel-title pull-left"><a href="?article=${article.id}"></a></h3>--%>

            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="..." alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">${article.title}</h5>
                    <p class="card-text">${article.content}</p>
                    <a href="#" class="btn btn-primary">Load Full Article</a>
                </div>
            </div>


            <%--</div>--%>
            <%--<div class="panel-body">--%>
            <%--<p>${article.content}</p>--%>
            <%--</div>--%>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <p>No articles!</p>
    </c:otherwise>
</c:choose>

<br>
<div class="container">
    <form action="/Article" method="get">
        <button type="submit" class="btn btn-primary my-2">Article</button>
    </form>
</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#registerModal"    >
    Register
</button>

<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#newArticleModal">
   Create New Article
</button>

<div class="modal fade" id="articleModal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true">



    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">-->
            <!--&lt;!&ndash;<h5 class="modal-title text-center" id="modal_title">Супер Боул мгновенно</h5>&ndash;&gt;-->
            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
            <!--<span aria-hidden="true">&times;</span>-->
            <!--</button>-->
            <!--</div>-->
            <div class="modal-body">

                <p class="modal_title"> Hello Sunshine</p>
                <p class="modal_txt">Есть много вариантов, но большинство из них имеет не всегда приемлемые модификации, например, юмористические
                    вставки или слова, которые даже отдалённо не напоминают латынь.
                    Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>

            </div>
            <div id="commentArea" style="background-color: #4CAF50">

                <p>Your Comment here...</p>
            </div>


            <div>
            <form>
                Edit:
                <textarea rows="5" cols="50" id="editTextArea">


                </textarea>
                <button type="button" class="btn btn-primary save" data-toggle="modal" id="#saveButton">Save</button>

                <p>Leave a comment!</p>
                        <textarea rows="2" cols="50" id="commentTextArea" >


            </textarea>
                <button type="button" class="btn btn-primary comment" data-toggle="modal" onclick="leaveComment()" id="commentButton">Comment</button>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="button" class="btn btn-primary edit" data-toggle="modal"  onclick="myFunction()">Edit Article</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="newArticleModal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true" style="width: 50%; height: 75%">

<%--<%@include file="editArticle.jsp"%>--%>

<div class="modal-content">
  <div class="modal-body">

    <form>

        <p>Please Enter a title</p>
        <input type="text" name="newTitle">

        <p>Content:</p>
        <textarea  rows="10" cols="50" name="newContent"></textarea>


    </form>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  </div>

</div>
</div>


<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true">
<div class="modal-content">
    <div class="modal-body">
    <%--TODO put this in a --%>
    <%--seperate jsp file--%>

        <%@include file="registration.jsp"%>
</div>
</div>
</div>



<script>
    $('#articleModal').on('show.bs.modal', function (event) {
        console.log("I'm working!");
        var button = $(event.relatedTarget); // Button that triggered the modal

        var titleData=button.data('title');
        var contentData=button.data('content');


                                  // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal_title').text(titleData);
        modal.find('.modal_txt').text(contentData);
        modal.find('#editTextArea').text(contentData);
    })


</script>


<script>
    function myFunction() {

       var x= $("#articleModal #editTextArea");
       console.log(x);
        x.css("display","block");

        var button=$("#articleModal .save");
        button.css("display","block");


       // if (x.css.display === "none") {
       //      x.css("display", "block");
       //  }
        // } else {
        //     x.css("display","none");
        // }


    }



</script>





<script>

function leaveComment() {
    var comment=$('#articleModal #commentTextArea');
    var myComment=comment.val();

    console.log(comment);

    var commentArea=  $("#articleModal #commentArea");

    commentArea.append("<div class=\"comment\" >"+myComment+"</div>");
    commentArea.append("<button type=\"button\" class=\"btn btn-primary smallCommentBtn\" onclick=\"leaveNestedComment()\" id=\"commentButton\">Comment</button><br>");
    commentArea.append("<textarea class=\"smallText\" rows='1' cols='35'></textarea> ");


    commentArea.css("background-color","grey","border-radius","30px");


}

function leaveNestedComment() {
    var comment=$('#articleModal #nestedComment');
    var myComment=comment.val();

    console.log(comment);

    var commentArea=  $("#articleModal #commentArea");

    commentArea.append("<div class=\"tinyComment\" >"+myComment+"</div>");
    commentArea.append("<button type=\"button\" class=\"btn btn-primary tinyCommentBtn\" onclick=\"leaveNestedComment()\" id=\"commentButton\">Comment</button><br>");
    commentArea.append("<textarea class=\"tinyText\" rows='1' cols='35'></textarea> ");


    console.log(document.getElementById("commentArea"))

}



</script>

</body>
</html>
