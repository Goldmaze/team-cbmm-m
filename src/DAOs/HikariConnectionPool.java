package DAOs;

import com.zaxxer.hikari.HikariDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class HikariConnectionPool {
    private static HikariDataSource hds;

    static {
//        Properties dbProps = new Properties();
//        try (FileInputStream fis = new FileInputStream("connection.properties")) {
//            dbProps.load(fis);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        hds = new HikariDataSource();
//        hds.setJdbcUrl(dbProps.getProperty("url"));
//        hds.setDriverClassName("com.mysql.jdbc.Driver");
//        hds.setUsername(dbProps.getProperty("user"));
//        hds.setPassword(dbProps.getProperty("password"));
//        hds.setMaximumPoolSize(2);

        hds = new HikariDataSource();
        hds.setJdbcUrl("jdbc:mysql://db.sporadic.nz:3306/clin864");
        hds.setDriverClassName("com.mysql.jdbc.Driver");
        hds.setUsername("clin864");
        hds.setPassword("DecideFrightenAirUpPerson");
        hds.setMaximumPoolSize(2);

    }

    public static Connection getConnection() throws SQLException {
        return hds.getConnection();
    }
}