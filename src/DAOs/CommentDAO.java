package DAOs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentDAO implements AutoCloseable {
    private final Connection conn;


    public CommentDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods
    public Comment getAllComments() throws SQLException {
        Comment comment = new Comment();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_comments")) {
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    comment.setCommentId(r.getInt(1));
                    comment.setUserId(r.getInt(2));
                    comment.setArticleId(r.getInt(3));
                    comment.setReferenceId(r.getInt(4));
                    comment.setContent(r.getString(5));
                }
                return comment;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return comment;
    }

    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
